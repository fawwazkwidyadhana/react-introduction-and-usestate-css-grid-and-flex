import React, { useState, useEffect } from "react";
import "./styles.css";

function App() {
  return (
    <div className="container">
      <div className="box square"></div>
      <div className="box rectangle"></div>
      <div className="box triangle"></div>
      <div className="box circle"></div>
      <div className="box rounded-square"></div>
      <div className="box rounded-rectangle"></div>
    </div>
  );
}

export default App;
